using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float horizontal;
    private float direction;
    private float moveSpeed = 5f;
    private bool canMove;
    private bool isDashing;
    /* Unity doesn't allow exposing private variables
    to the Inspector, if we want to do that 
    anyway, we can use [SerializeField] */
    [SerializeField] private float dashVelocity = 20f;
    /* Create some effects after dash 
    using object pooling technique*/
    public List<GameObject> afterImages;
    [SerializeField] private GameObject afterImagesPrefab;
    [SerializeField] private short numberOfAfterImages = 10;
    [SerializeField] private float afterImageCooldown = 0.01f;
    private float afterImageTimer = 0;
    private bool isJumping;
    private bool isFalling;
    private Animator animator;
    private new Rigidbody2D rigidbody2D;
    private SpriteRenderer spriteRenderer;
    private float defaultGravity;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        defaultGravity = rigidbody2D.gravityScale;
        canMove = true;

        afterImages = new List<GameObject>(numberOfAfterImages);
        for (int i=0;i<numberOfAfterImages;i++)
        {
            var afterImage = Instantiate(afterImagesPrefab);
            afterImage.SetActive(false);
            afterImages.Add(afterImage);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Get keyboard value
        // A will be -1 and D will be 1
        horizontal = Input.GetAxisRaw("Horizontal");
        // Change the variable inside animator
        animator.SetFloat("Speed", Math.Abs(horizontal));
        // Set the direction of player only when
        // keys are typed
        if (horizontal != 0)
        {
            direction = horizontal;
            animator.SetFloat("Direction", direction);
        }

        // Jump and Fall
        {
            // Check if we clicked the space button
            if (Input.GetKeyDown(KeyCode.Space)) HandleJumping();

            // If vertical velocity < 0, we are falling, otherwise
            // we are not
            if (rigidbody2D.velocity.y == 0)
            {
                isFalling = false;
                animator.SetBool("Fall", isFalling);
            }
            else if (rigidbody2D.velocity.y < 0) HandleFalling();
        }
        
        // Check if we clicked left mouse button
        if (Input.GetKeyDown(KeyCode.Mouse1)) HandleDashing();
    }

    // FixedUpdate is called once every fixedDeltaTime (0.02s)
    public void FixedUpdate()
    {
        HandleMoving();
        // Create some effects if we dash
        HandleAfterImage();
    }

    public void HandleMoving()
    {
        if (canMove) transform.position += new Vector3(horizontal * moveSpeed * Time.fixedDeltaTime, 0);
    }

    public void HandleJumping()
    {
        // Add a vertical force to the player
        rigidbody2D.AddForce(new Vector2(0, 10), ForceMode2D.Impulse);
        isJumping = true;
        animator.SetBool("Jump", isJumping);
    }

    public void HandleFalling()
    {
        isJumping = false;
        isFalling = true;
        animator.SetBool("Fall", isFalling);
        animator.SetBool("Jump", isJumping);
    }

    public void HandleDashing()
    {
        // Temporarily remove gravity and every velocites
        rigidbody2D.gravityScale = 0;
        // Add a horizontal velocity to player
        rigidbody2D.velocity = new Vector2(dashVelocity*direction, 0);
        isDashing = true;
        animator.SetBool("Dash", isDashing);
        canMove = false;
    }

    public void HandleAfterImage()
    {
        /* create effect only if we are dashing
        and the cooldown is reached */
        if (isDashing && afterImageTimer > afterImageCooldown)
        {
            CreateAfterImage();
        }
        else afterImageTimer += Time.fixedDeltaTime;
    }

    public void CreateAfterImage()
    {
        /* Create effect using object pooling
        technique */
        GameObject afterImage = null;
        for (int i=0;i<numberOfAfterImages;i++)
        {
            afterImage = afterImages[i];
            if (!afterImage.activeSelf) break;
        }

        if (afterImage != null)
        {
            afterImage.GetComponent<SpriteRenderer>().sprite = spriteRenderer.sprite;
            afterImage.transform.position = transform.position;
            afterImage.transform.localScale = new Vector3(spriteRenderer.flipX ? -1 : 1, 1, 1);
            afterImage.SetActive(true);
        }
    }

    // After dash animation end, we call this
    // See dash animation tab for more detail
    private void AfterDash()
    {
        rigidbody2D.velocity = Vector2.zero;
        rigidbody2D.gravityScale = defaultGravity;
        isDashing = false;
        canMove = true;
        animator.SetBool("Dash", isDashing);
    }
}
