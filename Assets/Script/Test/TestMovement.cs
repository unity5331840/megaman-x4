using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovement : MonoBehaviour
{
    private float horizontal;
    private float direction;
    private float moveSpeed = 5f;
    private Animator animator;
    private new Rigidbody2D rigidbody2D;
    private float defaultGravity;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        defaultGravity = rigidbody2D.gravityScale;
    }

    // Update is called once per frame
    void Update()
    {
        // Get keyboard value
        // A will be -1 and D will be 1
        horizontal = Input.GetAxisRaw("Horizontal");
        // Change the variable inside animator
        animator.SetFloat("Speed", Math.Abs(horizontal));
        // Set the direction of player only when
        // keys are typed
        if (horizontal != 0)
        {
            direction = horizontal;
            animator.SetFloat("Direction", direction);
        }

        // Check if we clicked the space button
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Add a vertical force to the player
            rigidbody2D.AddForce(new Vector2(0, 10), ForceMode2D.Impulse);
            animator.SetBool("Jump", true);
        }
        
        // Check if we clicked left mouse button
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            // Temporarily remove gravity and every velocites
            rigidbody2D.gravityScale = 0;
            rigidbody2D.velocity = Vector2.zero;
            // Add a horizontal velocity to player
            rigidbody2D.velocity = new Vector2(10*direction, 0);
            animator.SetBool("Dash", true);
        }
        
        // If vertical velocity < 0, we are falling, otherwise
        // we are not
        if (rigidbody2D.velocity.y == 0)
        {
            animator.SetBool("Fall", false);
        }
        else if (rigidbody2D.velocity.y < 0)
        {
            animator.SetBool("Fall", true);
            animator.SetBool("Jump", false);
        }
    }

    // FixedUpdate is called once every fixedDeltaTime (0.02s)
    private void FixedUpdate()
    {
        transform.position += new Vector3(horizontal * moveSpeed * Time.fixedDeltaTime, 0);
    }

    // After we dashed, we use 
    private void AfterDash()
    {
        animator.SetBool("Dash", false);
        rigidbody2D.gravityScale = defaultGravity;
        rigidbody2D.velocity = Vector2.zero;
    }
}
